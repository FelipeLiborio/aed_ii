#ifndef __GRAFO_H__
#define __GRAFO_H__

typedef struct grafo{
    int eh_ponderado, nro_vertices, grau_max;
    int **arestas;
    float **pesos;
    int *grau;
}Grafo;

Grafo *cria_Grafo(int nro_vertices, int grau_max, int eh_ponderado);
void libera_Grafo(Grafo *gr);
int insereAresta(Grafo *gr, int orig, int dest, int eh_digrafo, float peso);
int removeAresta(Grafo *gr, int orig, int dest, int eh_digrafo);
void buscaProfundidade_Grafo(Grafo *gr, int ini, int *visitado);
void buscaLargura_Grafo(Grafo *gr, int ini, int *visitado);

#endif
