#include<stdio.h>
#include<stdlib.h>
#include "menu.h"
#include "Grafo.h"

static char grafo_existente = 0;

void menu(){
    Grafo *gr = NULL;
    menu_inicio();
}

void KAKAROTTO(){
        printf("--------------------------------------------------------------------------------\
------------------------------------KAKAROTTO-----------------------------------\
--------------------------------------------------------------------------------\n");
}

void menu_novo_Grafo_manual(Grafo *gr);
void menu_inserir_aresta_manual(Grafo *gr);
void menu_novo_Grafo_arquivo(Grafo *gr);
void menu_inserir_arestas_arquivo(Grafo *gr);
void menu_exibir_tabela_vertice(Grafo *gr);
void menu_exibir_todas_as_tabelas(Grafo *gr);
void criar_arquivo_tabelas(Grafo *gr);
void menu_remover_aresta(Grafo *gr);
void MASTER();

void menu_inicio(Grafo *gr){
    char escolha, escolhido = 0, i;
    while(!escolhido){
        escolhido = 1;
        system("cls");
        KAKAROTTO();
        printf("\t\t\t\t    DIJKSTRA\n\n\
Escolha:\n\n1 - Criar grafo manualmente\n2 - Inserir aresta manualmente\n3 - Criar grafo a partir de\
 um arquivo\n4 - Inserir arestas a partir de um arquivo\n5 - Exibir tabela Dijkstra para um v�rtice\n\
6 - Exibir tabelas Dijkstra de todos os v�rtices\n7 - Criar arquivo com tabelas Dijkstra para todos os\
 v�rtices\n8 - Remover aresta\n9 - Liberar grafo\n0 - Sair\n>>>");
        scanf("%d", &escolha);
        switch(escolha){
            case 0:
                printf("1 - Confirma\nOUTRO - Cancela\n");
                scanf("%d", &i);
                escolhido = (i == 1) ? 1:0;
                if(i == 1) exit(1);
                break;
            case 1:
                if(!grafo_existente){
                    grafo_existente = 1;
                    menu_novo_Grafo_manual(gr);
                }
                else{
                    printf("J� existe um grafo criado! Escolha liberar grafo\n");
                    system("pause");
                    escolhido = 0;
                }
                break;
            case 2:
                menu_inserir_aresta_manual(gr);
                break;
            case 3:
                if(!grafo_existente){
                    grafo_existente = 1;
                    menu_novo_Grafo_arquivo(gr);
                }
                else{
                    printf("J� existe um grafo criado! Escolha liberar grafo\n");
                    system("pause");
                    escolhido = 0;
                }
                break;
            case 4:
                menu_inserir_arestas_arquivo(gr);
                break;
            case 5:
                menu_exibir_tabela_vertice(gr);
                break;
            case 6:
                menu_exibir_todas_as_tabelas(gr);
                break;
            case 7:
                criar_arquivo_tabelas(gr);
                escolhido = 0;
                break;
            case 8:
                menu_remover_aresta(gr);
                break;
            case 9:
                libera_Grafo(gr);
                printf("Grafo apagado.\n");
                escolhido = 0;
                grafo_existente = 0;
                system("pause");
                break;
            case 107:
                MASTER();
                escolhido = 0;
                break;
            default:
                printf("Escolha inv�lida!\n");
                system("pause");
                escolhido = 0;
        }
    }
}

void menu_novo_Grafo_manual(Grafo *gr){
    system("cls");
    KAKAROTTO();
    int v, g, p;
    printf("Insira:\n[N�mero de v�rtices] [Grau m�ximo] [� ponderado?(1 ou 0)]\nExemplo: 55 10 0\n>>>");
    scanf("%d %d %d", &v, &g, &p);
    if(v > 0 && g > 0){
        gr = cria_Grafo(v, g, p);
        printf("Criado\n");
        system("pause");
    }
    else{
        printf("Entrada inv�lida!\n");
        system("pause");
        menu_novo_Grafo_manual(gr);
    }
    menu_inicio(gr);
}

void menu_inserir_aresta_manual(Grafo *gr){
    system("cls");
    KAKAROTTO();
    int orig, dest, digrafo;
    float peso = 1;
    printf("Insira:\n[V�rtice de origem] [V�rtice de destino] [� orientada?(1 ou 0)]");
    if(gr->eh_ponderado) printf(" [Peso( >= 0 )");
    printf(" - Exemplo: 0 5 0");
    if(gr->eh_ponderado) printf(" 13,7");
    printf("\n>>>");
    scanf("%d %d %d", &orig, &dest, &digrafo);
    if(gr->eh_ponderado) scanf("%f", &peso);
    if(orig >= 0 && orig < gr->nro_vertices
       && dest >= 0 && dest < gr->nro_vertices
       && (digrafo == 1 || digrafo == 0)
       && peso >= 0){
        insereAresta(gr, orig, dest, digrafo, peso);
        printf("Inserida\n");
        system("pause");
    }
    else{
        printf("Entrada inv�lida!\n");
        system("pause");
        menu_inserir_aresta_manual(gr);
    }
    menu_inicio(gr);
}

void menu_novo_Grafo_arquivo(Grafo *gr){
    system("cls");
    KAKAROTTO();
    FILE *file;
    printf("Informe o endere�o de um arquivo que contenha o grafo a ser inserido\n\n\
A primeira linha do arquivo dever� estar no seguinte formato:\n\
[N�mero de v�rtices] [Grau m�ximo] [� ponderado?(1 ou 0)]\nExemplo: 55 10 0\n\
Cada linha seguuinte, se houver, ir� conter uma aresta e dever� estar no seguinte formato\n\
(O programa travar� caso contr�rio):\n");
    printf("[V�rtice de origem] [V�rtice de destino] [� orientada?(1 ou 0)] [Peso( >= 0)](Caso seja ponderado)\n");
    printf("Endere�o do arquivo(Ex.: 'ackerman.mikasa':\n>>>");
    char arquivo[256];
    scanf("%s", &arquivo);
    file = fopen(arquivo, "r");
    if(file == NULL){
        printf("Arquivo n�o encontrado!\n");
        system("pause");
        menu_novo_Grafo_arquivo(gr);
    }else{
        int v, g, p;
        if(!feof(file)){
            fscanf(file, "%d %d %d", &v, &g, &p);
            gr = cria_Grafo(v, g, p);
            inserir_arestas_arquivo(gr, file);
        }
        else{
            printf("Arquivo vazio!");
            fclose(file);
            system("pause");
            menu_inicio(gr);
        }
        fclose(file);
    }
    printf("Inseridas\n");
    system("pause");
    menu_inicio(gr);
}

void inserir_arestas_arquivo(Grafo *gr, FILE *file){
    int orig, dest, digrafo;
    float peso = 1;
    while(1){
        if(feof(file)) break;
        if(gr->eh_ponderado){
            fscanf(file, "%d %d %d %f", &orig, &dest, &digrafo, &peso);
        }else{
            fscanf("%d %d %d", &orig, &dest, &digrafo);
        }
        if(orig >= 0 && orig < gr->nro_vertices
           && dest >= 0 && dest < gr->nro_vertices
           && (digrafo == 1 || digrafo == 0)
           && peso >= 0){
            insereAresta(gr, orig, dest, digrafo, peso);
       }
    }
}

void menu_inserir_arestas_arquivo(Grafo *gr){
    system("cls");
    KAKAROTTO();
    FILE *file;
    printf("Informe o endere�o de um arquivo que contenha as arestas a serem inseridas\n\n\
Cada linha do arquivo dever� estar no seguinte formato(O programa travar� caso contr�rio):\n");
    if(gr->eh_ponderado) printf("[V�rtice de origem] [V�rtice de destino] [� orientada?(1 ou 0)] [Peso( >= 0)]\n");
    else printf("[V�rtice de origem] [V�rtice de destino] [� orientada?(1 ou 0)]\n");
    printf("Endere�o do arquivo(Ex.: 'ackerman.mikasa':\n>>>");
    char arquivo[256];
    scanf("%s", &arquivo);
    file = fopen(arquivo, "r");
    if(file == NULL){
        printf("Arquivo n�o encontrado!\n");
        system("pause");
        menu_inserir_arestas_arquivo(gr);
    }else{
        inserir_arestas_arquivo(gr, file);
        fclose(file);
    }
    printf("Inseridas\n");
    system("pause");
    menu_inicio(gr);
}

void menu_exibir_tabela_vertice(Grafo *gr){
    system("cls");
    KAKAROTTO();
    float dist[gr->nro_vertices];
    int orig, ant[gr->nro_vertices];
    printf("Insira o v�rtice de origem >>>");
    scanf("%d", &orig);
    if(orig >= 0 && orig < gr->nro_vertices){
        menorCaminho_Grafo(gr, orig, ant, dist);
        int i;
        printf("V�rtice\tAnterior\tDist�ncia\n");
        for(i = 0; i < gr->nro_vertices; i++){
            printf("%d\t%d\t%f\n", i, ant[i], dist[i]);
        }
        system("pause");
    }
    else{
        printf("Entrada inv�lida!\n");
        system("pause");
        menu_exibir_tabela_vertice(gr);
    }
    menu_inicio(gr);
}

void menu_exibir_todas_as_tabelas(Grafo *gr){
    system("cls");
    KAKAROTTO();
    int i;
    float dist[gr->nro_vertices];
    int ant[gr->nro_vertices];
    printf("Tabelas de dist�ncia para todos os v�rtices:\n");
    for(i = 0; i < gr->nro_vertices; i++){
        menorCaminho_Grafo(gr, i, ant, dist);
        int j;
        printf("V�rtice\tAnterior\tDist�ncia\t\tDist�ncias do v�rtice %d\n", i);
        for(j = 0; j < gr->nro_vertices; j++){
            printf("%d\t%d\t%f\n", j, ant[j], dist[j]);
        }
        printf("\n\n");
    }
    system("pause");
    menu_inicio(gr);
}

void criar_arquivo_tabelas(Grafo *gr){
    system("cls");
    KAKAROTTO();
    int i;
    if(gr == NULL){
        printf("Grafo inexistente!\n");
        system("pause");
        menu_inicio(gr);
    }
    float dist[gr->nro_vertices];
    int ant[gr->nro_vertices];
    printf("Voc� pode especificar o endere�o do arquivo durante a inser��o do nome.\n\
Insira o nome e a extens�o do arquivo(Ex.: ackerman.mikasa): ");
    char nome[256];
    scanf("%s", &nome);
    FILE *file;
    file = fopen(nome, "w");
    fprintf(file, "Tabelas de dist�ncia para todos os v�rtices:\n");
    for(i = 0; i < gr->nro_vertices; i++){
        menorCaminho_Grafo(gr, i, ant, dist);
        int j;
        fprintf(file, "V�rtice\tAnterior\tDist�ncia\t\tDist�ncias do v�rtice %d\n", i);
        for(j = 0; j < gr->nro_vertices; j++){
            fprintf(file, "%d\t%d\t%f\n", j, ant[j], dist[j]);
        }
        fprintf(file, "\n\n");
    }
    fclose(file);
    system("pause");
    menu_inicio(gr);
}

void menu_remover_aresta(Grafo *gr){
    system("cls");
    KAKAROTTO();
    int orig, dest, digrafo;
    printf("Informe: [V�rtice de origem] [V�rtice de destino] [� orientada?(1 ou 0)]\n>>>");
    scanf("%d %d %d", &orig, &dest, &digrafo);
    if(orig >= 0 && orig < gr->nro_vertices && dest >= 0 && dest < gr->nro_vertices && (digrafo == 0 | digrafo == 1)){
        if(removeAresta(gr, orig, dest, digrafo)) printf("Removida\n");
        else printf("N�o encontrada\n");}
    else{
        printf("Entrada inv�lida!\n");
        system("pause");
        menu_remover_aresta(gr);
    }
    system("pause");
    menu_inicio(gr);
}

void MASTER(){
    system("cls");
    KAKAROTTO();
    printf("\
???????.........................................:........:+I.I?.:::::::,I??????.\
????????=..........................:::.......??::.........:::.............III??.\
?????III::.......................I:::......????::.....................I????????.\
??I,:::::::....................?=:::.....???...,:..................?I??????????.\
I:::::::.................:....???::.....I??.+?=::...?............::::::::.?????.\
::::....................:,...=.?,::....???.?III::...?....................:::::..\
::.....................::...=???I::...??IIIIII?:..????........................:.\
......................=::..=?????::..??IIIII77:..????...........................\
......................=::..=?????::.???IIII  +   7??+...........................\
......................=?,:.     ?7:.I?IIII7       ?????.........................\
.......................??..      I:IIIIIII        7???....7..................??.\
.....................:.??~~       IIIIIII7        +?????.I?I.............??????.\
.....................??7I~        7IIIIII7..      ,?????I~??.........,?????????.\
......................?III       ..IIIIII7..     7??????I???......:????????????.\
.................7II7.IIII7      . 7IIIII777IIIIII?????.????....+++++++++++++++.\
++~..............II?IIIIIII   IIIIIIIIIIIIIIIIIIIII,=????+??........+++++++++++.\
++++++++++=......III.IIIIIII?I?????IIII7IIIIIIIIIIII?????I?...........,++++++++.\
===============..IIII+=III77IIIIIIIIIII=.?IIIII?III???.=I?..............=======.\
=============,:...I7+??IIIIIIII?IIIIII===III77IIIII???II??=====================.\
===========:.......II??=IIIIIII?II7IIII7+77777IIII??????=======================.\
~~~~~~~~=:..........II7+.IIIIIIII777777.......IIII???I?~~~~~~~~~~~~~~~~~~~~~~~~.\
~~~~~~~...~~~~~~~~~~~.III:IIIIIIII.......=~~.:IIII?????~~~~~~~~~~~~~~~~~~~~~~~~.\
~~~~~~~~~~~~~~~~~~~~~~~~~7IIIIIIII=.~??????~~IIII??????,~~~~~~~~~~~~~~~~~~~~~~~.\
~,:.~~~~~~~~~~~~~~~~~~~~~7IIIIIIIII.?????.77IIIII??I????~~~~~~~~~~~~~~~~~~~~~~~.\
~+++++++=~~++~~~~~~~~~~~~7II7IIIIIIIIIIIIIIIIIII?I7?????~~~~~~~~~~~~~~~~~~~~~~~.\
~~~~.~~~.~~~~~~~~~~~~~~~~7IIIIIIIIIIIII+~?IIIII?==I?????==??~~~~~~,::::,~~~~~~~.\
,::::::~~..:~~~~~~~~~~~~~7IIIIII=IIIIIIIIIIIII?==I??????.===???..:.,::::::,::::.\
~,~~~~..~,~,~~~~~~~~~~~~.7IIIIIII?=IIIIIIIIII===,I??????==~==????,::.::::::::,:.\
~~~~~~~~~~~~~~~~~~~~~~III~IIIIIII7??==IIIII=====I?+??????====?????:::.::::::::,.\
~~~~~~~~~~~~~~=:~,::I???.=IIIII?II=???======???.??=????????????????:::.::::::::.\
~~~~~~~~~~====,:::????I==?IIIII?,II:II?????????I??=????????????????:::.=:::::::.\
~~~~=========.:::????=???IIIII??=II7.IIIIII???II??==??????+?????????::,.=::::::.\
................................................................................\
");
    system("pause");
}

